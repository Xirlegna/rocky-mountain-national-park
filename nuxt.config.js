export default {
  target: 'static',

  head: {
    title: 'rocky-mountain-national-park',
    htmlAttrs: { lang: 'en' },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.png' }
    ]
  },

  styleResources: {
    scss: [
      '~/assets/sass/abstracts/mixins.scss'
    ],
  },

  css: ['~/assets/sass/app.scss'],

  plugins: ['~/plugins/language.js'],

  components: true,

  buildModules: [],

  modules: ['@nuxtjs/i18n'],

  i18n: {
    locales: ['en', 'hu'],
    defaultLocale: 'en',
    langDir: 'lang/',
    locales: [
      { code: 'en', file: 'en.json', iso: 'en' },
      { code: 'hu', file: 'hu.json', iso: 'hu' },
    ],
    vueI18n: { fallbackLocale: 'en' }
  },

  build: {
  }
}
